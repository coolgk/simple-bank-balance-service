const config = require('../config');
const Bigtable = require('@google-cloud/bigtable');

class DB {
    static getInstance () {
        return new Bigtable({
            projectId: config.db.projectId,
        }).instance(config.db.instanceId);
    }
}

module.exports = DB;
