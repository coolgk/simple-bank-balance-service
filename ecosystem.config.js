module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [
    {
      name      : 'Transaction Service',
      script    : 'index.js',
      instances: -1, // 0 to spread the app across all CPUs, -1 to spread the app across all CPUs - 1
      exec_mode: 'cluster',
      max_memory_restart: '1G',
      error_file: '.pm2/logs/err.log', // (default to $HOME/.pm2/logs/XXXerr.log)
      out_file: '.pm2/logs/out.log', // (default to $HOME/.pm2/logs/XXXout.log)
    }
  ],
};
