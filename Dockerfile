FROM node:8

RUN useradd nodeapp

RUN mkdir /node-app
COPY . /node-app/.
WORKDIR /node-app

# for using pm2 to run node as a different user
ENV PM2_HOME=/node-app/.pm2
RUN chown -R nodeapp /node-app

RUN npm install

EXPOSE 3000

CMD ["/bin/su", "-c", "npm start", "nodeapp"]
